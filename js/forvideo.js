$(document).on('click','.video-go',function(){
//    $('.video-frame-2').css('opacity', 0);
      $('.video-frame-2').animate({opacity:1}, 250);
    var start = Date.now();

    var timer = setInterval(function() {
      var timePassed = Date.now() - start;

      if (timePassed >= 250) {
        $('.video-frame-2').width('100%');
        $('.video-frame-2').height('100%');
        $('.video-frame-2').css("top","0%");
        $('.video-frame-2').css("left","0%");
        clearInterval(timer);
        return false;
      }
      show(timePassed);
    }, 20);
    function show(timePassed) {
        $('.video-frame-2').show();

//        $('.video-frame-2').fadeTo("slow", 1);
        $('.video-frame-2').css("left",(50-(timePassed / 5) + '%'));
        $('.video-frame-2').width((timePassed / 2.5) + '%');
        $('.video-frame-2').css("top",(50-(timePassed / 5) + '%'));
        $('.video-frame-2').height((timePassed / 2.5) + '%');

    }
});

$(document).mousedown(function (e){

  $('.video-frame-2').animate({opacity:0.2}, 250);
    var div = $("#videoframe");
    if (!div.is(e.target) && $('.video-frame-2').is(':visible')) {

        var start = Date.now();

        var timer = setInterval(function() {
          var timePassed = Date.now() - start;

          if (timePassed >= 250) {
            $('.video-frame-2').width('0%');
            $('.video-frame-2').height('0%');
            $('.video-frame-2').css("top","50%");
            $('.video-frame-2').css("left","50%");
            $('.video-frame-2').hide();
            clearInterval(timer);
            return false;
          }

          hide(timePassed);
        }, 20);
        function hide(timePassed) {

            $('.video-frame-2').css("left",((timePassed / 5) + '%'));
            $('.video-frame-2').width(100-(timePassed / 2.5) + '%');
            $('.video-frame-2').css("top",((timePassed / 5) + '%'));
            $('.video-frame-2').height(100-(timePassed / 2.5) + '%');


        }

    }
});
