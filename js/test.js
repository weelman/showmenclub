$('.mensphoto').on('click', function () {
    $(this).find('img').animate({opacity: 0.5}, 50);
    $(this).find('img').animate({opacity: 1}, 100);
    $(this).find('img').animate({opacity: 0.6}, 100);
    $(this).find('img').animate({opacity: 1}, 100);
    $(this).find('img').animate({opacity: 0.6}, 100);
    $(this).find('img').animate({opacity: 1}, 100, function() {
        document.location = $(this).parent().data('link');
        console.log($(this).parent().data('link'));
    });
    return false;
});
