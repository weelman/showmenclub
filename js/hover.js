$('.mensphoto').on('mouseenter', function () {
    $(this).find('img').attr('src', '/img/' + $(this).find('img').data('largeimg'));
    $(this).find('h4').html($(this).find('img').data('name'));
    $(this).find('h4').fadeIn(400);
    var i = 0;
    $(this).find('img').animate({opacity: 0.92}, 210);
    $(this).find('img').animate({opacity: 1}, 210);
});

$('.mensphoto').on('mouseleave', function () {
    $(this).find('img').attr('src', '/img/' + $(this).find('img').data('smallimg'));
    $(this).find('h4').fadeOut(50);
});


